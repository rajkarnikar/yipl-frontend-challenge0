
import React, { useState }  from 'react';
import logo from './frontendninja.png';


import './stylenavbar.css';

 function Navbar() {
    const [open, setOpen] = useState(!true); // for burger menu
    const [txt, settxt] = useState(!true);
    
    
          
    return(
        <>
                   
                    
             <nav className="navbar" >
                                                   
                
                    <div className="logo"><a href="landing"  >
                            <img src={logo} alt="Logo" height={45}   />
                    </a></div>
                   
                        
                        <ul className="pannel">
                            
                        <li>
                                
                            </li>
                        <li><ul className="nav-links" style={{transform: open ?"translateY(0px)" : "" }}>   
                            
                            
                            <li><a href="landing" className="gr"  >
                                Home
                            </a></li>
                            
                            <li>
                                <a href="about" className="a"  >
                                    About 
                                </a>
                                
                            </li>
                            <li>
                                <a href="work" className="a"  >
                                    work 
                                </a>
                                
                            </li>
                            <li>
                                <a href="contact" className="a"  >
                                    Contact
                                </a>
                                
                            </li>
                            <li>
                                <div className="arrangelogo">
                                            
                                    
                                    <a href="#" className="a"  >
                                        <i onClick={() =>settxt(!txt)}  class="fas fa-search"></i>
                                    </a>  
                                </div>
                                
                            </li>
                          <div className="search" style={{transform: txt ?"translateY(0px)" : "" }}> 
                                <a href="#" className="hide"  >
                                    <i onClick={() =>settxt(!txt)}  class="fas fa-search"></i>
                                </a>  
                                <input className="pholder"   placeholder=" Search"/>                                       
                            </div>
                                        
                        </ul></li>
                        
                          
                         
                        </ul>   
                        
                        <i onClick={() =>setOpen(!open)} className="fas fa-bars burger" ></i> 
                    
                    
                                       
                                                                               
            </nav>
           
        </>    
            )  
    }

export default Navbar

import React from 'react'
import Hero from './Hero';
import './Landing.css'
import Cardsection from './Cardsection';
import Information from './Information';
import Ninja from './Ninja';
import Foods from './Foods';
import Facility from './Facility';

export default function Landing() {
    
           
         return(
            <div >
             <Hero />   
             <Cardsection />
             <Information />
             <Ninja />
             <Foods />
             <Facility />
            </div>
         )
                
                
        
} 

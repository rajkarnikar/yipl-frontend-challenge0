import React from 'react'
import './Cardsection.css'
import card1 from './Images/card1.png';
import card2 from './Images/card2.png';
import card3 from './Images/card3.png';
export default function Cardsection() {
    
           
         return(
            <div className="maint" >
                <div className="containerb">
                    <h1 className="title" >What you should have</h1>
                </div> 
                <div class="row">
                    <div class="column">
                        <div class="card">
                            <img className="pic" src={card1}  />
                            <p1 className="cardtitle">Attention to details</p1><br/>
                            <div className="cardinfo">
                                <p1  >We look for attention to detail in our candidates. As a frontend intern, you should have a superpowers of looking into details of a design.</p1>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="card"> 
                            <img className="pic" src={card2}  />
                            <p1 className="cardtitle">Front tech knoledge</p1><br/>
                            <div className="cardinfo">
                                <p1  >You must be equipped with a passion to learn and explore new technologies. You are not afraid to implement a new framework or language.</p1>
                            </div>
                            
                        </div>
                    </div>
                    <div class="column">
                        <div class="card">
                            <img className="pic" src={card3}  />
                            <p1 className="cardtitle">Love for technology</p1><br/>
                            <div className="cardinfo">
                                <p1  >We eat, sleep and breathe tehcnology. You should have love for technology. </p1>
                            </div>
                        </div>
                    </div>
                    
                </div>
            {/* <div class="grid-container">
                <div class="grid-item">
                    <img className="pic" src={card1}  /> 
                </div>
                <div className="pic" class="grid-item">
                    <img src={card2}  /> 
                </div>
                <div className="pic" class="grid-item">
                    <img src={card3}  /> 
                </div>
            </div> */}
              
            </div>
         )
                
                
        
} 

import React from 'react'
import './Facility.css'
import Tailored from './Images/tailored.png';
import a  from './Images/1.png';
import b  from './Images/2.png';
import c from './Images/3.png';
import d from './Images/4.png';
import e  from './Images/5.png';
import f  from './Images/6.png';
import g  from './Images/7.png';
import h  from './Images/8.png';
import lightninja from './Images/Ninja-light.png';
import {useState , useEffect} from 'react';
import Popup from'../Popup';

export default function Facility() {
  const [buttonPopup, setButtonPopup] = useState(false);    
           
         return(
            <div className="bg-colour" >
              <h1 className="f-header" >What we provide</h1>
              <div className="f-box">
                <div class="f-grid-container">
                  <div class="f-grid-item">
                  <div class="i-grid-container">
                    <div class="i-grid-item">
                    <img className="f-icon" src={Tailored}  />
                    </div>
                    <div class="i-grid-item">
                      TAILORED OBJECT 
                      <br/><br/>
                      <p1 >
                      A small river named Duden flows by their place and supplies it with the necessary regelialia.
                      </p1>
                    </div>                   
                  </div>
                  </div>
                  <div class="f-grid-item">
                  <div class="i-grid-container">
                  <div class="i-grid-item">
                    <img className="f-icon" src={g}  />
                    </div>
                    <div class="i-grid-item">
                      SUPPORT FROM SENIOR TEAM 
                      <br/><br/>
                      <p1 >
                      A small river named Duden flows by their place and supplies it with the necessary regelialia.
                      </p1> 
                    </div>                   
                  </div>
                  </div>
                  <div class="f-grid-item">
                  <div class="i-grid-container">
                  <div class="i-grid-item">
                    <img className="f-icon" src={h}  />
                    </div>
                    <div class="i-grid-item">
                      LEARNING ENVIORNMENT  
                      <br/><br/>
                      <p1 >
                      A small river named Duden flows by their place and supplies it with the necessary regelialia.
                      </p1>
                    </div>                   
                  </div>
                  </div>  
                  <div class="f-grid-item">
                  <div class="i-grid-container">
                  <div class="i-grid-item">
                    <img className="f-icon" src={f}  />
                    </div>
                    <div class="i-grid-item">
                      DELICIOUS LUNCH   
                      <br/><br/>
                      <p1 >
                      A small river named Duden flows by their place and supplies it with the necessary regelialia.
                      </p1>
                    </div>                   
                  </div>
                  </div>
                  <div class="f-grid-item">
                  <div class="i-grid-container">
                  <div class="i-grid-item">
                    <img className="f-icon" src={c}  />
                    </div>
                    <div class="i-grid-item">
                      TRAVEL EXPENSES/STIPEND
                      <br/><br/>
                      <p1 >
                      A small river named Duden flows by their place and supplies it with the necessary regelialia.
                      </p1>
                    </div>                   
                  </div>
                  </div>  
                  <div class="f-grid-item">
                  <div class="i-grid-container">
                  <div class="i-grid-item">
                    <img className="f-icon" src={d}  />
                    </div>
                    <div class="i-grid-item">
                    FIVE WORKING WEEK
                    <br/><br/>
                      <p1 >
                      A small river named Duden flows by their place and supplies it with the necessary regelialia.
                      </p1>
                    </div>                   
                  </div>
                  </div>
                  <div class="f-grid-item">
                  <div class="i-grid-container">
                  <div class="i-grid-item">
                    <img className="f-icon" src={e}  />
                    </div>
                    <div class="i-grid-item">
                      FLEXIBLE TIME POSSIBLITY
                      <br/><br/>
                      <p1 >
                      A small river named Duden flows by their place and supplies it with the necessary regelialia.
                      </p1>
                    </div>                   
                  </div>
                  </div>  
                  <div class="f-grid-item">
                  <div class="i-grid-container">
                  <div class="i-grid-item">
                    <img className="f-icon" src={b}  />
                    </div>
                    <div class="i-grid-item">
                      
                        FULL-TIMER POSSIBILITY
                        <br/><br/>
                      <p1 >
                      A small river named Duden flows by their place and supplies it with the necessary regelialia.
                      </p1>
                      
                    </div>                   
                  </div>
                  </div>
                  <div class="f-grid-item">
                  <div class="i-grid-container">
                  <div class="i-grid-item">
                    <img className="f-icon" src={a}  />
                    </div>
                    <div class="i-grid-item">
                      SOCIALS,RETREATS,TEAM DINNERS
                      <br/><br/>
                      <p1 >
                      A small river named Duden flows by their place and supplies it with the necessary regelialia.
                      </p1>
                    </div>                   
                  </div>
                  </div>
                   
            </div>
                </div>
                <button className="b-btn">
                  <div className="but-container">                   
                        <div className="f-button" onClick={()=>setButtonPopup(true)}>
                                      APPLY NOW    <i class="fas fa-chevron-circle-right"></i>
                          </div>                      
                    </div>
                  </button>
                  <Popup className="popup" trigger={buttonPopup} setTrigger={setButtonPopup}>
                                <h1>Are you ready??</h1>
                                <p1>Join our crew of frontend ninjas</p1>
                                <form  className="form" action="/apply">
                                    <input  type="text" id="fname" name="name" placeholder="Your name.."/><br/>
                                    <input type="email" id="email" name="email" placeholder="E-mail address"></input><br/>
                                    <input type="submit" value="Submit"></input>
                                </form>
                            </Popup>
                <div className="l-ninja-container">
                  <img className="l-ninja" src={lightninja}  />
                </div>
              </div>
            
         )
                
                
        
} 

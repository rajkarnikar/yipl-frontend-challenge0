import React from 'react'
import './Foods.css'
import Food1 from './Images/Food1.png';
import Food2 from './Images/Food2.png';
import Food3 from './Images/Food3.png';
import Food4 from './Images/Food4.png';
import Food5 from './Images/Food5.png';
import Food6 from './Images/Food6.png';
export default function Foods() {
    
           
         return(
            <div className="tot-area" >
                <div className="txt-area">
                    <h1 className="txt-h">Some random food photos</h1>
                    <p1 className="txt-p">Just because ¯\_(ツ)_/¯</p1>
                </div>
                <div class="food-container">
                    <div class="food-item">
                        <img className="food" src={Food1}  />    
                    </div>
                    <div class="food-item">
                        <img className="food" src={Food2}  />    
                    </div>
                    <div class="food-item">
                        <img className="food" src={Food3}  />    
                    </div>
                    <div class="food-item">
                        <img className="food" src={Food4}  />    
                    </div>
                    <div class="food-item">
                        <img className="food" src={Food5}  />    
                    </div>
                    <div class="food-item">
                        <img className="food" src={Food6}  />    
                    </div>
                    
                </div>
              </div>
            
         )
                
                
        
} 

import React from 'react'
import './Information.css'

export default function Information() {
    
           
         return(
            <div className="section" >
                <div class="part">
                    <div class="left">
                    <div className="infoleft">
                        <h2 className="header-left">Resources</h2> 
                            <ul>
                                <li className="listleft">
                                  <a href="#" className="for-color" > Designer News</a> 
                                  
                                </li >
                                
                                <li className="listleft">
                                  <a href="#" className="for-color"> Smashing magazine</a>
                                  
                                </li>
                               
                                <li className="listleft">
                                  <a href="#" className="for-color">Frontend Front</a>
                                  
                                </li>
                                <a href="#"><li className="listlast">View all resources</li></a>
                            </ul>
                           
                        </div>
                    </div>
                    
                    <div class="right">
                        <div className="inforight">
                            <h2 className="header-right">Latest from frontend</h2>
                            <ul>
                                <li className="listright">
                                  <a href="#" className="for-color" > JavaScript Performance Optimization Tips: An Overview</a> 
                                  
                                </li >
                                
                                <li className="listright">
                                  <a href="#" className="for-color"> Designing a dashboard for Concourse</a>
                                  
                                </li>
                               
                                <li className="listright">
                                  <a href="#" className="for-color">JSRobot – Learn to code in JavaScript by playing a game</a>
                                  
                                </li>
                                <a href="#"><li className="listlast">View all Updates</li></a>
                            </ul>
                        </div>
                    </div>
                </div>
               
                
              
            </div>
         )
                
                
        
} 

import React from 'react'
import './Hero.css' 
import bg from './Images/bg.jpg';
import Popup from'../Popup';
import {useState , useEffect} from 'react';
 function Hero() {
    const [buttonPopup, setButtonPopup] = useState(false);
    const [timedPopup, setTimedPopup] = useState(false);  
    
    useEffect(()=>{
        setTimeout(()=>{
        setTimedPopup(true);
    },3000);
    },[]) ;   
         return(
            <div>
                <div class="container">
                    <img src={bg}   className="hero-img" />
                    
                    <div class="centered">
                        <h1 className="bold">Are you a frontend ninja?</h1><br/>
                        <p1 className="small"> Frontend ninjas are the superheroes who bring life into designs. We’re looking for frontend ninjas with knack of amazing attention to detail. Are you one? </p1>
                         <button>  
                            <div className="button" onClick={()=>setButtonPopup(true)} > 
                                
                                    APPLY NOW    <i class="fas fa-chevron-circle-right"></i>
                                
                            </div></button> 
                            <Popup className="popup" trigger={buttonPopup} setTrigger={setButtonPopup}>
                                <h1>Are you ready??</h1>
                                <p1>Join our crew of frontend ninjas</p1>
                                <form  className="form" action="/aapply">
                                    <input  type="text" id="fname" name="name" placeholder="Your name.."/><br/>
                                    <input type="email" id="email" name="email" placeholder="E-mail address"></input><br/>
                                    <input type="submit" value="Submit"></input>
                                </form>
                            </Popup>
                            <Popup className="popup"  trigger={timedPopup} setTrigger={setTimedPopup}>
                                <h1>Are you ready??</h1>
                                <p1>Join our crew of frontend ninjas</p1>
                                <form className="form" action="/apply">
                                <input type="text" id="fname" name="name" placeholder="Your name.."/><br/>
                                <input type="email" id="email" name="email" placeholder="E-mail address"></input><br/>
                                <input type="submit" value="Submit"></input>
                                </form>
                            </Popup>
                           
                            
                        
                    </div>
                </div>
            </div>
            
         )
                
                
        
} 
export default Hero;

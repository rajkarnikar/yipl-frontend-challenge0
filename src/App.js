import React from 'react';
import Navbar from "./Components/Navbar/Navbar";
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Footer from "./Components/Footer/Footer";
import Landing from './Components/Pages/Landing';
import Apply from './Components/Pages/Apply';
import About from './Components/Pages/About';
import Contact from './Components/Pages/Contact';
import Work from './Components/Pages/Work';
import './App.css';

function App() {
  return (<div>
        <Router>     
          
          <Navbar />
            <Switch>
              <Route exact path='/'  component={Landing} />
              <Route path='/landing'  component={Landing} />
              <Route path='/apply'  component={Apply} />
              <Route path='/about'  component={About} />
              <Route path='/contact'  component={Contact} />
              <Route path='/work'  component={Work} />
              
            </Switch> 
        </Router>
        <Footer />        
  </div>
    
  );
}

export default App;
